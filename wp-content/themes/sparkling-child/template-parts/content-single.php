<div class="single-portada interna-portada d-flex align-items-center">
	<?php the_post_thumbnail(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
	<div class="overlay"></div>
</div>

<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row">
			<div class="col-xs-12 col-12 col-md-7 post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div>
			<div class="col-xs-12 col-12 col-md-5">

				<div class="post-content">
					<div class="section-title">
						<h1><?php the_title(); ?></h1>
						<hr>
					</div>
					<?php the_content(); ?>
					<a href="obras" class="item-button d-flex align-items-center justify-content-center">Volver a obras</a>
					<?php
						wp_link_pages(
							array(
								'before'      => '<div class="page-links">' . esc_html__( 'Pages:', 'sparkling' ),
								'after'       => '</div>',
								'link_before' => '<span>',
								'link_after'  => '</span>',
								'pagelink'    => '%',
								'echo'        => 1,
							)
						);
					?>
				</div><!-- .entry-content -->

				<footer class="entry-meta">

					<?php if ( has_tag() ) : ?>
				<!-- tags -->
				<div class="tagcloud">

						<?php
						$tags = get_the_tags( get_the_ID() );
						foreach ( $tags as $tag ) {
							echo '<a href="' . get_tag_link( $tag->term_id ) . '">' . $tag->name . '</a> ';
						}
						?>

				</div>
				<!-- end tags -->
					<?php endif; ?>

				</footer><!-- .entry-meta -->
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-12">
				<div class="row contacto-container d-flex align-items-center">
					<div class="col-xs-12 col-md-7 single-image-container d-flex align-items-center">
						<img src="http://item.com.ar/wp-content/uploads/2018/10/single-contacto.jpg" alt="Item S.A">
					</div>
					<div class="col-xs-12 col-md-5 text-container d-flex align-self-stretch flex-column">
						<div class="section-title">
							<h1>Contacto</h1>
							<hr>
						</div>
						<?php echo do_shortcode('[contact-form-7 id="51" title="Formulario de contacto"]') ?>
					</div>
				</div>
			</div>
		</div>

		<?php if ( get_the_author_meta( 'description' ) ) : ?>
			<div class="post-inner-content secondary-content-box">
		<!-- author bio -->
		<div class="author-bio content-box-inner">

			<!-- avatar -->
			<div class="avatar">
				<?php echo get_avatar( get_the_author_meta( 'ID' ), '60' ); ?>
			</div>
			<!-- end avatar -->

			<!-- user bio -->
			<div class="author-bio-content">

			<h4 class="author-name"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></h4>
			<p class="author-description">
					<?php echo get_the_author_meta( 'description' ); ?>
			</p>

			</div><!-- end .author-bio-content -->

		</div><!-- end .author-bio  -->

			</div>
			<?php endif; ?>

	</article><!-- #post-## -->
</div>