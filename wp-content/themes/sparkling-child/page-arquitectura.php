<?php
/**
 * Template Name: Arquitectura
 *
 * This is the template that displays full width page without sidebar
 *
 * @package sparkling
 */

get_header(); ?>

    <div id="obras" class="obras-section">
        <div class="interna-portada arquitectura-portada d-flex align-items-center justify-content-start">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-12">
                        <h1>Arquitectura</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="obras-container row d-flex align-items-stretch">
                <?php
                    $query_jobs = new WP_Query( 'category_name=arquitectura' );
                    while ($query_jobs -> have_posts()) : $query_jobs -> the_post();
                    $totalJobs = $query_jobs->found_posts;
                ?>
                <div class="col-xs-12 col-12 col-md-4 d-flex align-self-stretch flex-column single-obra">
                    <div class="obra-header d-flex align-items-center justify-content-center">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="obra-body d-flex flex-column">
                        <h1><?php the_title(); ?></h1>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink();?>" class="item-button d-flex align-items-center justify-content-center">Ver más</a>
                    </div>
                </div>
                <?php 
                    endwhile;
                    wp_reset_postdata();
                ?>
                    
            </div>
        </div>
    </div>

<?php get_footer(); ?>