<?php
/**
 * Template Name: Nosotros
 *
 * This is the template that displays full width page without sidebar
 *
 * @package sparkling
 */

get_header(); ?>

    <div id="obras" class="nosotros-section nosotros-interna">
        <div class="interna-portada nosotros-portada d-flex align-items-center justify-content-start">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-12">
                        <h1>Sobre nosotros</h1>
                    </div>
                </div>
            </div>
        </div>

        <!-- INTRO -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-lg-8 mx-auto text-center intro">
                    <p>Item Construcciones es una empresa constructora radicada en la Provincia de Córdoba, Argentina, que nace a raíz de la amistad de un grupo de ingenieros que en marzo de 1970, deciden formalizar su sociedad y constituir una S.R.L. Once años después, la responsabilidad y el compromiso implicado en cada uno de sus proyectos, motoriza el crecimiento y la especialización de su equipo de trabajo, lo que resulta, en marzo de 1981, en la constitución como Sociedad Anónima.</p>
                    <p>Construcción de viviendas, líneas de transmisión de alta tensión y estaciones transformadoras, redes de distribución de electricidad, gas, agua, cloacas, telecomunicaciones y obras de infraestructura vial e hidráulica, son algunas de las áreas en las cuales se desempeña.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-12 col-md-4 text-center nosotros-col">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/mision.png">
                    <div class="section-title text-center mx-auto">
                        <h1>Misión</h1>
                        <hr>
                    </div>
                    <p>Desarrollar grandes proyectos de infraestructura electromecánica, vial y de arquitectura para comitentes públicos y privados en Córdoba y el Interior del País, valiéndose de la más alta tecnología y la solvencia y excelencia profesional de nuestro capital humano.</p>
                </div>
                <div class="col-xs-12 col-12 col-md-4 text-center nosotros-col">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/valores.png">
                    <div class="section-title text-center mx-auto">
                        <h1>Valores</h1>
                        <hr>
                    </div>
                    <p>Integridad<br>Profesionalismo<br>Calidad<br>Compromiso<br>Mejora continua<br>Flexibilidad<br>Responsabilidad</p>
                </div>
                <div class="col-xs-12 col-12 col-md-4 text-center nosotros-col">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/vision.png">
                    <div class="section-title text-center mx-auto">
                        <h1>Visión</h1>
                        <hr>
                    </div>
                    <p>Constituirnos en una empresa líder en la industria de la construcción argentina, ser reconocida por la calidad humana de nuestros colaboradores y por la confiabilidad, transparencia, eficiencia y competitividad en la satisfacción de las expectativas y demandas de nuestros clientes.</p>
                </div>
            </div>

        </div><!-- FIN INTRO -->

        <!-- SERVICIOS -->
        <div class="servicios-container d-flex align-items-center">
            <div class="text-container d-flex align-self-stretch flex-column">
                <hr>
                <div class="services-list">
                    <h1>En los últimos 15 años</h1>
                    <div class="d-flex align-items-start">
                        <span>·</span>
                        <p>3000 viviendas construidas</p>
                    </div>
                    <div class="d-flex align-items-start">
                        <span>·</span>
                        <p>+ de 250.000 m2 cubiertos</p>
                    </div>
                    <div class="d-flex align-items-start">
                        <span>·</span>
                        <p>+ de 7 Líneas Aéreas de Alta Tensión</p>
                    </div>
                    <div class="d-flex align-items-start">
                        <span>·</span>
                        <p>+ de 4 Líneas Subterráneas de Alta Tensión</p>
                    </div>
                    <div class="d-flex align-items-start">
                        <span>·</span>
                        <p>8 Estaciones Transformadoras</p>
                    </div>
                    <div class="d-flex align-items-start">
                        <span>·</span>
                        <p>8 Tendidos de Fibra Óptica</p>
                    </div>
                </div>
            </div>
            <div class="image-container d-flex align-items-center">
                <img src="http://item.com.ar/wp-content/uploads/2018/10/servicios.jpg" alt="Item S.A">
            </div>
        </div><!-- FIN SERVICIOS -->

        <!-- HISTORIA -->
        <div class="container">
            <div class="presentacion-container d-flex align-items-center">
                <div class="image-container d-flex align-items-center">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/archive2.jpg" alt="Item S.A">
                </div>
                <div class="text-container d-flex align-self-stretch flex-column">
                    <hr>
                    <div class="presentacion-text">
                        <h1>MEDIO SIGLO DE TRAYECTORIA EN LA INDUSTRIA DE LA CONSTRUCCIÓN</h1>
                    </div>
                </div>
            </div>
        </div><!-- FIN HISTORIA -->
        
    </div>

<?php get_footer(); ?>