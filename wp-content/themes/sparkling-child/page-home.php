<?php

/**
 * Template Name: Home
 *
 * This is the template that displays full width page without sidebar
 *
 * @package sparkling
 */

get_header(); ?>

    <!-- PORTADA -->
    <div id="portadaCarousel" class="carousel slide portada-slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators hidden-xs">
            <li data-target="#portadaCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#portadaCarousel" data-slide-to="1"></li>
            <li data-target="#portadaCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="http://item.com.ar/wp-content/uploads/2018/10/portada1.jpg">
            </div>
            <div class="item">
                <img src="http://item.com.ar/wp-content/uploads/2018/10/portada2.jpg">
            </div>
            <div class="item">
                <img src="http://item.com.ar/wp-content/uploads/2018/10/portada3.jpg">
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control visible-xs" href="#portadaCarousel" role="button" data-slide="prev">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control visible-xs" href="#portadaCarousel" role="button" data-slide="next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div><!-- FIN PORTADA -->

    <!-- NOSOTROS -->
    <div id="empresa" class="section nosotros-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-12 text-center">
                    <div class="section-title text-center mx-auto">
                        <h1>Sobre Nosotros</h1>
                        <hr>
                    </div>
                </div>
                <div class="col-xs-12 col-md-10 col-lg-8 mx-auto text-center">
                    <p>Construcción de viviendas, líneas de transmisión de alta tensión y estaciones transformadoras, redes de distribución de electricidad, gas, agua, cloacas, telecomunicaciones y obras de infraestructura vial e hidráulica, son algunas de las áreas en las cuales se desempeña.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-12 text-center">
                    <a href="nosotros" class="item-button d-flex align-items-center justify-content-center mx-auto">Ver más</a>
                </div>
            </div>
        </div>
    </div><!-- FIN NOSOTROS -->

    <!-- SERVICIOS -->
    <div class="servicios-container d-flex align-items-center">
        <div class="image-container d-flex align-items-center">
            <img src="http://item.com.ar/wp-content/uploads/2018/10/servicios.jpg" alt="Item S.A">
        </div>
        <div class="text-container d-flex align-self-stretch flex-column">
            <hr>
            <div class="services-list">
                <div class="d-flex align-items-start">
                    <span>·</span>
                    <p>Ejecución de contratos de gran envergadura, con comitentes públicos y privados</p>
                </div>
                <div class="d-flex align-items-start">
                    <span>·</span>
                    <p>Excelente imagen frente a proveedores y contratistas</p>
                </div>
                <div class="d-flex align-items-start">
                    <span>·</span>
                    <p>Cumplimiento en calidad y plazos de las obligaciones</p>
                </div>
                <div class="d-flex align-items-start">
                    <span>·</span>
                    <p>Gran capacidad instalada</p>
                </div>
                <div class="d-flex align-items-start">
                    <span>·</span>
                    <p>Adaptación y rápida respuesta</p>
                </div>
            </div>
        </div>
    </div><!-- FIN SERVICIOS -->

    <!-- HISTORIA -->
    <div class="section yellow-section historia-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-12 col-md-4 text-center single-historia">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/electromagneticas-2.png" class="mx-auto">
                    <p>Más de <span>81</span> obras<br> electromecánicas<br> ejecutadas</p>
                </div>
                <div class="col-xs-12 col-12 col-md-4 text-center single-historia">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/arquitectura.png" class="mx-auto">
                    <p>Más de <span>43</span> obras<br> de arquitectura, vivienda e<br> infraestructura ejecutadas</p>
                </div>
                <div class="col-xs-12 col-12 col-md-4 text-center single-historia">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/obras-viales.png" class="mx-auto">
                    <p>Más de <span>16</span><br> obras viales<br> ejecutadas</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-12 col-md-4 text-center single-historia">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/hidraulicas.png" class="mx-auto">
                    <p>Más de <span>26</span><br> obras hidráulicas<br> ejecutadas</p>
                </div>
                <div class="col-xs-12 col-12 col-md-4 text-center single-historia">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/telecomunicaciones.png" class="mx-auto">
                    <p>Más de <span>52</span> obras<br> de telecomunicaciones<br> ejecutadas</p>
                </div>
                <div class="col-xs-12 col-12 col-md-4 text-center single-historia">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/gasiferas.png" class="mx-auto">
                    <p>Más de <span>13</span> obras<br> de infraestructura gasífera<br> ejecutadas</p>
                </div>
            </div>
        </div>
    </div><!-- FIN HISTORIA -->

    <!-- OBRAS -->
    <div id="obras" class="section obras-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-12 text-center">
                    <div class="section-title text-center mx-auto">
                        <h1>Obras</h1>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-work d-flex align-items-center">
            <a class="work-category arquitectura-category" href="obras-arquitectura">
                <div class="section-title">
                    <h1>Arquitectura</h1>
                    <hr>
                </div>
            </a>
            <a class="work-category ingenieria-category" href="obras-ingenieria">
                <div class="section-title">
                    <h1>Ingenieria</h1>
                    <hr>
                </div>
            </a>
        </div>
        <div class="container">
            <div class="row d-flex align-items-stretch flex-wrap obras-row">
                <?php
                $query_jobs = new WP_Query('category_name=obras&posts_per_page=3');
                while ($query_jobs->have_posts()) : $query_jobs->the_post();
                $totalJobs = $query_jobs->found_posts;
                ?>
                <div class="col-xs-12 col-12 col-md-4 d-flex align-self-stretch flex-column single-obra">
                    <div class="obra-header d-flex align-items-center justify-content-center">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="obra-body d-flex flex-column">
                        <h1><?php the_title(); ?></h1>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" class="item-button d-flex align-items-center justify-content-center">Ver más</a>
                    </div>
                </div>
                <div id="totalJobs" style="display: none;"><?php echo $totalJobs; ?></div>
                <?php 
                endwhile;
                wp_reset_postdata();
                ?>
            </div>

            <div class="row">

                <div class="col-xs-12 text-center load-more" id="loadMoreJobs">
                    <a href="obras" class="item-button d-flex align-items-center justify-content-center mx-auto">Ver todas</a>
                    <script>
                        var maxJobs = 3;
                        var totalJobs = jQuery('#totalJobs').text();
                        if ( totalJobs > maxJobs ) {
                            jQuery('#loadMoreJobs').css({
                                display: 'block'
                            });
                        }
                    </script>
                </div>
                    
            </div>
            <div class="row">
                <div class="col-xs-12 col-12 download-file">
                    <a href="http://item.com.ar/wp-content/uploads/2018/11/Item_Obras.pdf" target="_blank" class="item-button d-flex align-items-center justify-content-center mx-auto">Descargar ficha técnica</a>
                </div>
            </div>
        </div>
    </div><!-- FIN ÚLTIMAS OBRAS -->

    <!-- PRESENTACIÓN -->
    <div class="container">
        <div class="presentacion-container d-flex align-items-center">
            <div class="text-container d-flex align-self-stretch flex-column">
                <hr>
                <div class="presentacion-text">
                    <p>Contamos con un staff fijo de más de un centenar de colaboradores, técnicos y profesionales altamente calificados.</p>
                    <p>El accionar y la gestión de la empresa se basan en el compromiso por la calidad, la responsabilidad en la ejecución, la ética, la integridad y el respeto por la sociedad y el medio ambiente.</p>
                    <p>Somos una de las empresas líderes de la industria de la construcción del interior de la República Argentina.</p>
                </div>
            </div>
            <div class="image-container d-flex align-items-center">
                <img src="http://item.com.ar/wp-content/uploads/2018/10/servicios.jpg" alt="Item S.A">
            </div>
        </div>
    </div><!-- FIN PRESENTACIÓN -->

    <!-- IMAGEN DIVISIÓN -->
    <img src="http://item.com.ar/wp-content/uploads/2018/10/divider.jpg" alt="Item S.A" class="divider">
    <!-- FIN IMAGEN DIVISIÓN -->

    <!-- CONTACTO -->
    <div class="section" id="contacto">
        <div class="container">
            <div class="contacto-container d-flex align-items-center">
                <div class="image-container d-flex align-items-center">
                    <img src="http://item.com.ar/wp-content/uploads/2018/10/servicios.jpg" alt="Item S.A">
                </div>
                <div class="text-container d-flex align-self-stretch flex-column">
                    <div class="section-title">
                        <h1>Contacto</h1>
                        <hr>
                    </div>
                    <?php echo do_shortcode('[contact-form-7 id="51" title="Formulario de contacto"]') ?>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN CONTACTO -->

<?php get_footer(); ?>