<?php
/**
 * Template Name: Obras
 *
 * This is the template that displays full width page without sidebar
 *
 * @package sparkling
 */

get_header(); ?>

    <div id="obras" class="section obras-section obras-interna">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-12 text-center">
                    <div class="section-title text-center mx-auto">
                        <h1>Nuestras Obras</h1>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-stretch flex-wrap obras-row">
                <?php
                    $query_jobs = new WP_Query( 'category_name=obras' );
                    while ($query_jobs -> have_posts()) : $query_jobs -> the_post();
                ?>
                <div class="col-xs-12 col-12 col-md-4 d-flex align-self-stretch flex-column single-obra">
                    <div class="obra-header d-flex align-items-center justify-content-center">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="obra-body d-flex flex-column">
                        <h1><?php the_title(); ?></h1>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink();?>" class="item-button d-flex align-items-center justify-content-center">Ver más</a>
                    </div>
                </div>
                <?php 
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>