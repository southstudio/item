<?php
/**
 * Template Name: Ingenieria
 *
 * This is the template that displays full width page without sidebar
 *
 * @package sparkling
 */

get_header(); ?>

    <div id="obras" class="obras-section">
        <div class="interna-portada ingenieria-portada d-flex align-items-center justify-content-start">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-12">
                        <h1>Ingenieria</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs d-flex align-items-center justify-content-between flex-wrap" role="tablist">
                        <li role="presentation" class="active"><a class="remove-sub-cat" href="#hidraulicas" aria-controls="hidraulicas" role="tab" data-toggle="tab">Hidráulicas</a></li>
                        <li role="presentation"><a class="remove-sub-cat" href="#viales" aria-controls="viales" role="tab" data-toggle="tab">Viales</a></li>
                        <li role="presentation"><a class="remove-sub-cat" href="#electromecanicas" aria-controls="electromecanicas" role="tab" data-toggle="tab">Electromecánicas</a></li>
                        <!--<li role="presentation"><a class="remove-sub-cat" href="#telefonia" aria-controls="telefonia" role="tab" data-toggle="tab">Telefonía</a></li>
                        <div class="electrica-cat">
                            <li class="d-flex" role="presentation"><a id="electricaCat" href="#electrica" aria-controls="electrica" role="tab" data-toggle="tab">Eléctrica</a></li>
                            <ul class="sub-categories d-flex align-items-center justify-content-between">
                                <li role="presentation"><a href="#lineasAltas" aria-controls="lineasAltas" role="tab" data-toggle="tab">Líneas alta tensión y estaciones transformadoras</a></li>
                                <li role="presentation"><a href="#lineasBajas" aria-controls="lineasBajas" role="tab" data-toggle="tab">Líneas media y baja tensión</a></li>
                                <li role="presentation"><a href="#electrificacion" aria-controls="electrificacion" role="tab" data-toggle="tab">Electrificación rural</a></li>
                                <li role="presentation"><a href="#iluminacion" aria-controls="iluminacion" role="tab" data-toggle="tab">Iluminación</a></li>
                            </ul>
                        </div>
                        <script>
                            jQuery( "#electricaCat" ).click(function(){
                                jQuery( ".sub-categories" ).toggleClass("show");
                            });
                            jQuery( ".remove-sub-cat" ).click(function(){
                                jQuery( ".sub-categories" ).removeClass("show");
                            });
                        </script>-->
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-12">
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <!-- OBRAS HIDRÁULICAS -->
                        <div role="tabpanel" class="tab-pane active" id="hidraulicas">
                            <div class="obras-container row d-flex align-items-stretch">
                                <?php
                                    $query_hidraulicas = new WP_Query( 'category_name=hidraulicas' );
                                    while ($query_hidraulicas -> have_posts()) : $query_hidraulicas -> the_post();
                                ?>
                                <div class="col-xs-12 col-12 col-md-4 d-flex align-self-stretch flex-column single-obra">
                                    <div class="obra-header d-flex align-items-center justify-content-center">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div class="obra-body d-flex flex-column">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_excerpt(); ?>
                                        <a href="<?php the_permalink();?>" class="item-button d-flex align-items-center justify-content-center">Ver más</a>
                                    </div>
                                </div>
                                <?php 
                                    endwhile;
                                    wp_reset_postdata();
                                ?>
                            </div>
                        </div>

                        <!-- OBRAS VIALES -->
                        <div role="tabpanel" class="tab-pane" id="viales">
                            <div class="row d-flex align-items-stretch">
                                <?php
                                    $query_viales = new WP_Query( 'category_name=viales' );
                                    while ($query_viales -> have_posts()) : $query_viales -> the_post();
                                ?>
                                <div class="col-xs-12 col-12 col-md-4 d-flex align-self-stretch flex-column single-obra">
                                    <div class="obra-header d-flex align-items-center justify-content-center">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div class="obra-body d-flex flex-column">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_excerpt(); ?>
                                        <a href="<?php the_permalink();?>" class="item-button d-flex align-items-center justify-content-center">Ver más</a>
                                    </div>
                                </div>
                                <?php 
                                    endwhile;
                                    wp_reset_postdata();
                                ?>
                            </div>
                        </div>

                        <!-- OBRAS ELECTROMECÁNICAS -->
                        <div role="tabpanel" class="tab-pane" id="electromecanicas">
                            <div class="row d-flex align-items-stretch">
                                <?php
                                    $query_electromecanicas = new WP_Query( 'category_name=electromecanicas' );
                                    while ($query_electromecanicas -> have_posts()) : $query_electromecanicas -> the_post();
                                ?>
                                <div class="col-xs-12 col-12 col-md-4 d-flex align-self-stretch flex-column single-obra">
                                    <div class="obra-header d-flex align-items-center justify-content-center">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div class="obra-body d-flex flex-column">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_excerpt(); ?>
                                        <a href="<?php the_permalink();?>" class="item-button d-flex align-items-center justify-content-center">Ver más</a>
                                    </div>
                                </div>
                                <?php 
                                    endwhile;
                                    wp_reset_postdata();
                                ?>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>