<?php
    //Enqueue child stylesheet
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
    function my_theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    }

    //Add feature image support
    add_theme_support( 'post-thumbnails' );

    //Shorter excerpts
    function wpdocs_custom_excerpt_length( $length ) {
        return 15;
    }
    add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
    
?>