<?php
/**
 * Template Name: Equipos
 *
 * This is the template that displays full width page without sidebar
 *
 * @package sparkling
 */

get_header(); ?>

    <div id="obras" class="equipos-section equipos-interna">
        <div class="interna-portada equipos-portada d-flex align-items-center justify-content-start">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-12">
                        <h1>Equipos</h1>
                    </div>
                </div>
            </div>
        </div>

        <!-- SLIDER -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-12 slider-container">
                    <div id="equiposSlider" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <!--<ol class="carousel-indicators">
                            <li data-target="#equiposSlider" data-slide-to="0" class="active"></li>
                            <li data-target="#equiposSlider" data-slide-to="1"></li>
                            <li data-target="#equiposSlider" data-slide-to="2"></li>
                        </ol>-->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                                $the_imagequery = new WP_Query( 'category_name=equipo&posts_per_page=0' );
                                while ($the_imagequery -> have_posts()) : $the_imagequery -> the_post();
                            ?>
                            <div class="item">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <?php
                                endwhile;
                                wp_reset_postdata();
                            ?>
                            <script>
                                jQuery('#equiposSlider .carousel-inner .item:first-child').addClass('active');
                            </script>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#equiposSlider" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a>
                        <a class="right carousel-control" href="#equiposSlider" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-12">
                    <a href="http://item.com.ar/wp-content/uploads/2018/11/Item_Equipos.pdf" target="_blank" class="item-button download-file d-flex align-items-center justify-content-center mx-auto">Descargar listado completo de equipos y rodados</a>
                </div>
            </div>
        </div>
        
    </div>

<?php get_footer(); ?>